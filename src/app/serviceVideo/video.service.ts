import { Injectable } from '@angular/core';
import { Video } from '../model/video';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  data: Video[];
  constructor(private http: Http) { }


  read() {
    return this.http.get('http://127.0.0.1:8000/api/video');
  }
  insert(data: Video) {
    return this.http.post('http://127.0.0.1:8000/api/video', data);

  }
  update(data: Video) {
    return this.http.put('http://127.0.0.1:8000/api/video/' + data.id, data);

  }
  delete(id) {
    return this.http.delete('http://127.0.0.1:8000/api/video/' + id);

  }
}
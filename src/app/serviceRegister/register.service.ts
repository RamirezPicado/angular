import { Injectable } from '@angular/core';
import { Register } from  '../model/register';
import {Http} from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
 data:Register[];
  constructor(private http : Http) { }
  read() {
    return this.http.get('http://127.0.0.1:8000/register');
}
insert(data:Register){
  return this.http.post('http://127.0.0.1:8000/register',data);

}
update(data:Register){
  return this.http.put('http://127.0.0.1:8000/register/'+data.id,data);

}
delete(id){
  return this.http.delete('http://127.0.0.1:8000/register/'+id);

}
}

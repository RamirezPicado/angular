import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { VideoComponent } from './video/video.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { UserComponent } from './user/user.component';
import { FormComponent } from './form/form.component';

const routes: Routes=[
    {path:'',component:UserComponent},
    {path:'user',component:UserComponent},
    {path:'form',component:FormComponent},
    {path:'register',component:RegisterComponent},
    {path:'video',component:VideoComponent},
    {path:'playlist',component:PlaylistComponent}
  ];
  
@NgModule({   
        imports: [RouterModule.forRoot(routes)],  
        exports: [RouterModule]
     }) 

 export class AppRoutingModule { }

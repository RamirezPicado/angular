import { Component, OnInit } from '@angular/core';
import {User} from  '../model/user';
import {UserService} from '../serviceUser/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  data:User[];
  //agregamos una variable cliente
  current_user:User;
  //agregamos una variable para las operaciones del crud
  crud_operation={is_new: false,is_visible: false}
  //agregamos en el contructor el servicio y lo iniciamos vacio 
  constructor(private service: UserService) 
  {
    
    this.data=[];
   }

  ngOnInit(): void {
  
    //leemos el registro de la base de dato
    this.service.read().subscribe(res=>{
      this.data = res.json();
      this.current_user = new User();
    });
  }
  //metodo news
    new(){
      this.current_user = new User();
      this.crud_operation.is_visible = true;
      this.crud_operation.is_new = true;
    }
    save(){
      if(this.crud_operation.is_new){
        this.service.insert(this.current_user).subscribe(res=>{
          this.current_user = new User();
          this.crud_operation.is_visible = false;
          this.ngOnInit();
        });

        return;
      }
      this.service.update(this.current_user).subscribe(res=>{
        this.current_user = new User();
        this.crud_operation.is_visible = false;
        this.ngOnInit();

      });


    }

    edit(row){
      this.crud_operation.is_visible=true;
      this.crud_operation.is_new =false;
      this.current_user = row;

    }
    delete(id){
      this.service.delete(id).subscribe(res=>{
        this.crud_operation.is_new =false;
        this.ngOnInit();
      });

    }
  

  

}

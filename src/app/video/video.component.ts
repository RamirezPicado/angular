import { Component, OnInit } from '@angular/core';
import { Video } from '../model/video';
import { VideoService } from '../serviceVideo/video.service';


@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  data: Video[];
  //agregamos una variable cliente
  current_video: Video;
  //agregamos una variable para las operaciones del crud
  crud_operation = { is_new: false, is_visible: false }
  //agregamos en el contructor el servicio y lo iniciamos vacio 

  constructor(private service: VideoService) { 
    this.data=[];
  }

  ngOnInit(): void {
    this.data = [];
    //leemos el registro de la base de datos
    this.service.read().subscribe(res => {
      this.data = res.json();
      this.current_video = new Video();
    });
  }
  //metodo news
  new() {
    this.current_video = new Video();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }
  save() {
    if (this.crud_operation.is_new) {
      this.service.insert(this.current_video).subscribe(res => {
        this.current_video = new Video();
        this.crud_operation.is_visible = false;
        this.ngOnInit();
      });

      return;
    }
    this.service.update(this.current_video).subscribe(res => {
      this.current_video = new Video();
      this.crud_operation.is_visible = false;
      this.ngOnInit();

    });


  }

  edit(row) {
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_video = row;

  }
  delete(id) {
    this.service.delete(id).subscribe(res => {
      this.crud_operation.is_new = false;
      this.ngOnInit();
    });

  }


}

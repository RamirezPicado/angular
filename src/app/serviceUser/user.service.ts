import { Injectable } from '@angular/core';
import { User } from  '../model/user';
import { Http } from "@angular/http";
//import {HttpClient} from '@angular/common/http';
//mport { Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  data:User[];
  constructor (private http : Http) { }
  /*
  read(): Observable<any> {
    return this.http.get<any>('http://127.0.0.1:8000/api/user',{ observe: 'response' });
  }*/
  
  read() {
      return this.http.get('http://127.0.0.1:8000/api/user');
  }
  insert(data:User){
    return this.http.post('http://127.0.0.1:8000/api/user',data);

  }
  update(data:User){
    return this.http.put('http://127.0.0.1:8000/api/user/'+data.id,data);

  }
  delete(id){
    return this.http.delete('http://127.0.0.1:8000/api/user/'+id);

  }
}

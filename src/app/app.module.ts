import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule} from '@angular/forms';
import {Route, RouterModule} from   '@angular/router';
import  {AppRoutingModule} from './app-routing.module';
//import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';


import {UserService} from './serviceUser/user.service';
import { RegisterService } from './serviceRegister/register.service';
import { VideoService } from './serviceVideo/video.service';
import { PlaylistService } from './servicePlaylist/playlist.service';

import {UserComponent} from './user/user.component';
import {RegisterComponent} from './register/register.component';
import {PlaylistComponent} from './playlist/playlist.component';
import {VideoComponent} from './video/video.component';
import {FormComponent} from './form/form.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
   AppComponent,
    UserComponent,
    FormComponent,
    RegisterComponent,
    VideoComponent,
    PlaylistComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule
    //HttpClientModule
  ],
  providers: [UserService,RegisterService,VideoService,PlaylistService],
  bootstrap: [AppComponent]
})
export class AppModule { }

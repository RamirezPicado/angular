import { Component, OnInit } from '@angular/core';
import { Register } from  '../model/register';
import { RegisterService } from '../serviceRegister/register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    data:Register[];
    //agregamos una variable cliente
    current_register:Register;
    //agregamos una variable para las operaciones del crud
    crud_operation={is_new: false,is_visible: false}
    //agregamos en el contructor el servicio y lo iniciamos vacio 
  constructor(private service:RegisterService) { 
    this.data=[];

  }

  ngOnInit(): void {
  }
  
    //metodo news
      new(){
        this.current_register = new Register();
        this.crud_operation.is_visible = true;
        this.crud_operation.is_new = true;
      }
      save(){
        if(this.crud_operation.is_new){
          this.service.insert(this.current_register).subscribe(res=>{
            this.current_register = new Register();
            this.crud_operation.is_visible = false;
            this.ngOnInit();
          });
  
          return;
        }
        this.service.update(this.current_register).subscribe(res=>{
          this.current_register = new Register();
          this.crud_operation.is_visible = false;
          this.ngOnInit();
  
        });
  
  
      }
  
      edit(row){
        this.crud_operation.is_visible=true;
        this.crud_operation.is_new =false;
        this.current_register = row;
  
      }
      delete(id){
        this.service.delete(id).subscribe(res=>{
          this.crud_operation.is_new =false;
          this.ngOnInit();
        });
  
      }
    
  

}
